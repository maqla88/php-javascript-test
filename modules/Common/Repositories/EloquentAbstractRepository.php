<?php

namespace Modules\Common\Repositories;

abstract class EloquentAbstractRepository implements AbstractRepositoryInterface
{

    protected $model;

    /**
     * Gets all items from database
     *
     * @param  $options    
     * @return void
     */
    public function getAllItems($options = array())
    {
        $orderBy  = isset($options['orderBy']) ? $options['orderBy'] : null;
        $paginate = isset($options['paginate']) ? $options['paginate'] : null;
        $count    = isset($options['count']) ? $options['count'] : null;
        $limit    = isset($options['limit']) ? $options['limit'] : null;

        $model = $this->getModel();
        if ($orderBy)
            $model = $model->orderBy('created_at', $orderBy);

        if ($paginate)
            return $model->paginate($paginate);

        if ($count)
            return $model->count();

        if ($limit)
            $model = $model->take($limit);
        return $model->get();
    }

    /**
     * Find item in database by id  
     *
     * @param  $id    
     * @return void
     */
    public function findItem($id)
    {
        $item = $this->getModel()->find($id);
        return ($item) ? $item : null;
    }

    /**
     * Find item in database by column name and value  
     *
     * @param  $name
     * @param  $value     
     * @return void
     */
    public function findItemBy($name, $value)
    {
        $item = $this->getModel()->where($name, $value)->first();
        return ($item) ? $item : null;
    }

    /**
     * Find items in database by column name and value  
     *
     * @param  $name
     * @param  $value
     * @param  $options
     * @return void
     */
    public function findItemsBy($name, $value, $options = array())
    {
        $orderBy  = isset($options['orderBy']) ? $options['orderBy'] : null;
        $paginate = isset($options['paginate']) ? $options['paginate'] : null;
        $count    = isset($options['count']) ? $options['count'] : null;
        $limit    = isset($options['limit']) ? $options['limit'] : null;

        $items = $this->getModel()->where($name, $value);

        if ($orderBy)
            $items = $items->orderBy('created_at', $orderBy);

        if ($paginate)
            return $items->paginate($paginate);

        if ($count)
            return $items->count();

        if ($limit)
            $items = $items->take($limit);

        return $items->get();
    }

    /**
     * Creates or updates the item in the database 
     *
     * @param  $data
     * @param  $id
     * @return void
     */
    public function storeOrUpdateItem($data, $id = null)
    {
        if (isset($data['_token']))
            unset($data['_token']);
        $item = $this->findItem($id);
        if ($item) {
            $item->update($data);
            return $item;
        } else {
            return $this->getModel()->create($data);
        }
        return null;
    }

    /**
     * Delete items by column id .
     *
     * @param  $name
     * @param  $value
     * @return bool|null
     */
    public function destroyItem($id)
    {
        $item = $this->findItem($id);
        if (!$item)
            return null;
        $item->delete();
        return true;
    }

    /**
     * Delete items by column name and value.
     *
     * @param  $name
     * @param  $value
     * @return bool|null
     */
    public function destroyItemsBy($name, $value)
    {
        return $this->getModel()->where($name, $value)->delete();
    }

    public abstract function getModel();
}
