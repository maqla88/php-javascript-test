<?php

namespace Modules\Common\Repositories;

interface AbstractRepositoryInterface
{

    public function getAllItems($options = array());

    public function findItem($id);

    public function findItemBy($name, $value);

    public function findItemsBy($name, $value);

    public function storeOrUpdateItem($data, $id = null);

    public function destroyItem($id);

    public function destroyItemsBy($name, $value);
}
