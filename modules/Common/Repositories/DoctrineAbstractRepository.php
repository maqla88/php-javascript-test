<?php

namespace Modules\Common\Repositories;

abstract class DoctrineAbstractRepository implements AbstractRepositoryInterface
{

    protected $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function getAllItems($options = array()) {
        ;
    }

    public function findItem($id) {
        ;
    }

    public function findItemBy($name, $value) {
        ;
    }

    public function findItemsBy($name, $value) {
        ;
    }

    public function storeOrUpdateItem($data, $id = null) {
        ;
    }

    public function destroyItem($id) {
        ;
    }

}
