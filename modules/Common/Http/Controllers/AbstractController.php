<?php

namespace Modules\User\Http\Controllers;

use Maqla\Http\Controllers\Controller;

abstract class AbstractController extends Controller {

    public function index() {
        return view('user::index');
    }

}
