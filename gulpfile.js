var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.assetsPath = 'public/angular/';
elixir.config.publicPath = elixir.config.assetsPath;

elixir(function (mix) {

    mix.copy('node_modules/ngstorage/ngStorage.min.js',
            'public/angular/js/vendor'
            );

    mix.styles([
        'vendor/bootstrap/css/bootstrap.min.css',
        'main.css'
    ]);

    mix.scripts([
        'vendor/angular/angular.min.js',
//        'vendor/angular/angular-ui-router.min.js',
//        'vendor/satellizer.min.js',
        'vendor/angular/angular-resource.js',
        'vendor/angular/angular-route.min.js',
        'vendor/angular/angular-animate.min.js',
        'vendor/angular/angular-cookies.min.js',
        'vendor/angular/loading-bar.js',
        'vendor/ngStorage.min.js',
        'vendor/angular/angular-messages.min.js',     
        'vendor/jquery/jquery.min.js',
        'vendor/bootstrap/js/bootstrap.min.js',
        'angular/app.js',
        'angular/directives/directives.js',
        'angular/services/auth.js',
        'angular/services/book.js',
        'angular/controllers/main.js',
        'angular/controllers/home.js',
        'angular/controllers/auth.js',
        'angular/controllers/book.js',
        'main.js'
    ]);

});
