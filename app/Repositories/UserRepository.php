<?php

namespace App\Repositories;

use Modules\Common\Repositories\EloquentAbstractRepository;

class UserRepository extends EloquentAbstractRepository
{

    protected $userModel;

    public function getModel() {
        if (!$this->userModel)
            $this->userModel = new \App\User();
        return $this->userModel;
    }

}
