<?php

namespace App\Repositories;

use Modules\Common\Repositories\EloquentAbstractRepository;

class BookRepository extends EloquentAbstractRepository
{

    protected $bookModel;

    public function getModel() {
        if (!$this->bookModel)
            $this->bookModel = new \App\Book();
        return $this->bookModel;
    }

}
