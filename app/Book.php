<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'books';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'author', 'user_id', 'year', 'language', 'original_language',
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

}
