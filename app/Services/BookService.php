<?php

namespace App\Services;

class BookService
{

    protected $bookRepo;

    public function all($options) {
        return $this->getBookRepo()->getAllItems($options);
    }

    public function findByUser($user, $options) {
        return $this->getBookRepo()->findItemsBy('user_id', $user->id, $options);
    }

    public function create($data) {
        return $this->getBookRepo()->storeOrUpdateItem($data);
    }
    
    public function update($data, $id) {
        return $this->getBookRepo()->storeOrUpdateItem($data, $id);
    }

    public function delete($book_id) {
        return $this->getBookRepo()->destroyItem($book_id);
    }

    protected function getBookRepo() {
        if (!$this->bookRepo)
            $this->bookRepo = new \App\Repositories\BookRepository();
        return $this->bookRepo;
    }

}
