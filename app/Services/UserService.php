<?php

namespace App\Services;

class UserService
{

    protected $userRepo;

    public function all() {
        $options = array(
            'orderBy' => 'DESC'
        );
        return $this->getUserRepo()->getAllItems($options);
    }

    public function create($data) {
        return $this->getUserRepo()->storeOrUpdateItem($data);
    }

    protected function getUserRepo() {
        if (!$this->userRepo)
            $this->userRepo = new \App\Repositories\UserRepository();
        return $this->userRepo;
    }

}
