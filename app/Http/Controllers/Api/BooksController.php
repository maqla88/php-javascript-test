<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;
use App\Http\Requests\BookRequest;
use App\Http\Controllers\Controller;

class BooksController extends Controller
{

    protected $bookService;

    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['index', 'show']]);
    }

    public function index() {
        $options = array(
            'orderBy' => 'DESC',
            'paginate' => 5,
        );
        return $this->getBookService()->all($options);
    }

    public function create() {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        return $this->getBookService()->findByUser($user, ['orderBy' => 'DESC']);
    }

    public function store(BookRequest $request) {       
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();
        $data['user_id'] = $user->id;

        try {
            $response = $this->getBookService()->create($data);
        } catch (\Exception $e) {
            return response()->json(['error' => $data], 500);
        }
        return response()->json(['success' => 'Successfully created a new book'], 200);
    }
    
    public function update(BookRequest $request, $id) {       
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $data = $request->all();
        $data['user_id'] = $user->id;

        try {
            $response = $this->getBookService()->update($data, $id);
        } catch (\Exception $e) {
            return response()->json(['error' => $data], 500);
        }
        return response()->json(['success' => 'Successfully updated a book'], 200);
    }

    public function destroy($id) {
         try {
            $response = $this->getBookService()->delete($id);
        } catch (\Exception $e) {
            return response()->json(['error' => $data], 500);
        }
        return response()->json(['success' => 'Successfully deleted a book'], 200);
    }

    protected function getBookService() {
        if (!$this->bookService)
            $this->bookService = new \App\Services\BookService();
        return $this->bookService;
    }

}
