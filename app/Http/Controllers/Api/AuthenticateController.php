<?php

namespace App\Http\Controllers\Api;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthenticateController extends Controller
{

    protected $userService;

    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['authenticate', 'register']]);
    }

    /**
     * Return the user
     *
     * @return Response
     */
    public function index() {
        // Retrieve all the users in the database and return them        
        $users = $this->getUserService()->all();
        return $users;
    }

    /**
     * Return a JWT
     *
     * @return Response
     */
    public function authenticate(Request $request) {
        $credentials = $request->only('email', 'password');        
        try {
            // verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Invalid Credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'Could not Create Token'], 500);
        }
        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    /**
     * Create User
     *
     * @return Response
     */
    public function register(Request $request) {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
        ]);

        try {
            $user = \App\User::create([
                        'username' => null,
                        'email' => $request->email,
                        'password' => bcrypt($request->password),
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'User with this email already exists.'], 500);
        }

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('token'));
    }

    protected function getUserService() {
        if (!$this->userService)
            $this->userService = new \App\Services\UserService();
        return $this->userService;
    }

}
