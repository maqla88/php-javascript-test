# Instruction on how to install this app

1. Clone the repository in the project path

2. Install composer

3. Create .env file and Add DB credentials
   cp .env.example .env

4. If on Linux give permission to the following folder (public, storage, bootstrap/cache)
    sudo chmod 777 /public /storage /bootstrap/cache -R

5. Run migration and install tables in DB
    php artisan migrate

6. Generate key
php artisan key:generate

Thats it! You can lunch the app now.
