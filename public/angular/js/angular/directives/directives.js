laravelApp.directive('booksPagination', function () {
    return{
        restrict: 'E',
        replace: true,
        template: '<ul class="pagination">' +
                '<li ng-show="currentPage != 1"><a href="javascript:void(0)" ng-click="getBooks(1)">&laquo;</a></li>' +
                '<li ng-show="currentPage != 1"><a href="javascript:void(0)" ng-click="getBooks(currentPage-1)">&lsaquo; Prev</a></li>' +
                '<li ng-repeat="i in range" ng-class="{active : currentPage == i}">' +
                '<a href="javascript:void(0)" ng-click="getBooks(i)">{{i}}</a>' +
                '</li>' +
                '<li ng-show="currentPage != totalPages"><a href="javascript:void(0)" ng-click="getBooks(currentPage+1)">Next &rsaquo;</a></li>' +
                '<li ng-show="currentPage != totalPages"><a href="javascript:void(0)" ng-click="getBooks(totalPages)">&raquo;</a></li>' +
                '</ul>'
    };
});

