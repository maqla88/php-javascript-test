hetApp.factory('Question', function ($http, URL) {


    return {
        get: function () {
            return  $http.get(URL + '/questions');
        },
        create: function (data, file) {

            return   $http({
                method: 'POST',
                url: URL + '/questions',
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                transformRequest: function (data) {
                    var formData = new FormData();
                    //need to convert our json object to a string version of json otherwise
                    // the browser will do a 'toString()' on the object which will result 
                    // in the value '[Object object]' on the server.
                    formData.append("name", data.model.name);
                    formData.append("city", data.model.city);
                    if (typeof data.model.city_since !== 'undefined')
                        formData.append("city_since", data.model.city_since);
                    if (typeof data.model.hometown !== 'undefined')
                        formData.append("hometown", data.model.hometown);
                    if (typeof data.model.url_image !== 'undefined')
                        formData.append("url_image", data.model.url_image);
                    if (typeof data.model.neighborhood !== 'undefined')
                        formData.append("neighborhood", data.model.neighborhood);
                    if (typeof data.model.question !== 'undefined')
                        formData.append("question", angular.toJson(data.model.question));
                    //now add all of the assigned files

                    //add each file to the form data and iteratively name them
                    if (typeof data.file !== 'undefined')
                        formData.append("profile_image", data.file);


                    return formData;
                },
                        data: {model: data, file: file}
            });
        }
    };

});
