laravelApp.factory('Book', function ($http, urls) {

    return {
        get: function (page, success, error) {
            return  $http.get(urls.BASE_API + '/books?page=' + page).success(success).error(error);
        },
        getUserBooks: function (success, error) {
            return  $http.get(urls.BASE_API + '/books/create').success(success).error(error);
        },
        create: function (data, success, error) {
            return $http.post(urls.BASE_API + '/books', data).success(success).error(error);
        },
        edit: function (id, data, success, error) {
            return  $http.put(urls.BASE_API + '/books/' + id, data).success(success).error(error);
        },
        delete: function (id, success, error) {
            return  $http.delete(urls.BASE_API + '/books/' + id).success(success).error(error);
        }
    };
});
