var laravelApp = angular.module('laravelApp', ['ngStorage', 'ngRoute', 'angular-loading-bar'])
        .constant('urls', {
            BASE: SITE_URL + '/',
            BASE_API: SITE_URL + '/api'
        });
laravelApp.config(['$routeProvider', '$httpProvider', '$locationProvider', function ($routeProvider, $httpProvider, $locationProvider) {
        $routeProvider.
                when('/', {
                    templateUrl: 'angular/views/home.html',
                    controller: 'HomeController as home'
                }).
                when('/signin', {
                    templateUrl: 'angular/views/singin.html',
                    controller: 'AuthController as auth'
                }).
                when('/signup', {
                    templateUrl: 'angular/views/signup.html',
                    controller: 'AuthController as auth'
                }).
                when('/books', {
                    templateUrl: 'angular/views/books.html',
                    controller: 'BookController as book'
                }).
                when('/user-books', {
                    templateUrl: 'angular/views/user-books.html',
                    controller: 'BookController as book'
                }).
                otherwise({
                    redirectTo: '/'
                });
        // use the HTML5 History API
        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if ($localStorage.token) {
                            config.headers.Authorization = 'Bearer ' + $localStorage.token;
                        }
                        return config;
                    },
                    'responseError': function (response) {
                        if (response.status === 401 || response.status === 403) {
                            delete $localStorage.token;
                            $location.path('/signin');
                        }
                        return $q.reject(response);
                    }
                };
            }]);
    }]).run(function ($rootScope, $location, $localStorage) {
    $rootScope.token = false;
    $rootScope.$on("$routeChangeStart", function (event, next) {
        if ($localStorage.token == null) {
            if (next.templateUrl === "angular/views/user-books.html") {
                $location.path("/signin");
            }
        }
    });
});