laravelApp.controller('BookController', function (Book, $scope, $timeout) {
    var self = this;

    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.books = [];
    $scope.userBooks = [];
    $scope.query = {};
    $scope.queryBy = 'name';
    $scope.createBook = {};
    self.method = true;

// get books 
    $scope.getBooks = function (pageNumber) {

        if (pageNumber === undefined) {
            pageNumber = '1';
        }

        Book.get(pageNumber, function (response) {
            $scope.books = response.data;
            $scope.totalPages = response.last_page;
            $scope.currentPage = response.current_page;
            // Pagination Range
            var pages = [];

            for (var i = 1; i <= response.last_page; i++) {
                pages.push(i);
            }

            $scope.range = pages;
        }, function (response) {
            console.log(response);
        });
    };
// get books by user

    $scope.getUserBooks = function () {
        Book.getUserBooks(function (response) {
            $scope.userBooks = response;
        }, function (response) {
            console.log(response);
        });
    }

    // edit a book

    $scope.edit = function (book) {
        self.method = false;
        $scope.editBook = book;
    }
    $scope.cancel = function () {
        self.method = true;
    }

    $scope.submitEdit = function () {
        var id = $scope.editBook.id;        
        Book.edit(id, $scope.editBook, function (response) {
            $('body').find('#notification').empty().append('<div class="alert alert-success" role="alert">' + response.success + '</div>');
            $scope.getUserBooks();
        }, function (response) {
            console.log(response);
        });
    }

    // create a book
    $scope.submitCreate = function () {         
        Book.create($scope.createBook, function (response) {
            $('body').find('#notification').empty().append('<div class="alert alert-success" role="alert">' + response.success + '</div>');
            $scope.getUserBooks();
             $scope.createBook = null;
        }, function (response) {
            $('body').find('#notification').empty().append('<div class="alert alert-danger" role="alert">' + response.error + '</div>');
        });
       
    };

// delete a book
    $scope.delete = function (id) {
        Book.delete(id, function (response) {
            $('body').find('#notification').empty().append('<div class="alert alert-success" role="alert">' + response.success + '</div>');
            $scope.getUserBooks();
        }, function (response) {
            console.log(response)
        });
    }

});


