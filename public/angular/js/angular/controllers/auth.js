laravelApp.controller('AuthController', function ($rootScope, $scope, $location, $localStorage, Auth) {
    var self = this;
    function successAuth(res) {
        $localStorage.token = res.token;
        $scope.$parent.token = $localStorage.token;
        $location.path('/user-books');
    }

    self.login = function () {

        var formData = {
            email: self.email,
            password: self.password
        };

        Auth.signin(formData, successAuth, function (data) {
            self.error = data.error;
        });
    };

    self.signup = function () {
        var formData = {
            email: self.email,
            password: self.password

        };
       
        Auth.signup(formData, successAuth, function (data) {
            var html = "";
            angular.forEach(data, function (index, value) {
                html += "<span class=\"help-block\">" + index + "</span>";
            });
            $('#error_wrap').append(html);
            self.error = true;
        });
    };

    $scope.$parent.token = $localStorage.token;
    $rootScope.tokenClaims = Auth.getTokenClaims();
});


