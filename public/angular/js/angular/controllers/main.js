laravelApp.controller('MainController', function (Auth, $scope, $localStorage, $location) {
    var self = this;

    // check url if active
    self.pageClass = function (path) {
        return (path === $location.path()) ? 'active' : '';
    };

    $scope.token = $localStorage.token;
    ;

    self.logout = function () {
        Auth.logout(function () {
            window.location = "/";
        });
    };
});


