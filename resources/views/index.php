<!DOCTYPE html>
<html lang="en" ng-app="laravelApp">
    <head>
        <base href="/">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

        <!-- Styles -->
        <link href="angular/css/all.css" rel="stylesheet" type="text/css"/>
        <script>
            var SITE_URL = "<?php echo url('/') ?>";
            var TOKEN = "<?php echo csrf_token(); ?>";
        </script>
        <style>
            body {
                font-family: 'Lato';
            }

            .fa-btn {
                margin-right: 6px;
            }
        </style>
    </head>
    <body id="app-layout" data-ng-controller="MainController as main">
        <!-- Navigation -->
        <div ng-include="'angular/partials/nav.html'"></div>
        <div class="container">
            <div id="notification"></div>
            <div ng-view></div>
        </div>
        <!-- JavaScripts -->       
        <script src="angular/js/all.js" type="text/javascript"></script>
    </body>
</html>
